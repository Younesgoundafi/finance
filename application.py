import os

from cs50 import SQL
from flask import Flask, flash, jsonify, redirect, render_template, request, session
from flask_session import Session
from tempfile import mkdtemp
from werkzeug.exceptions import default_exceptions, HTTPException, InternalServerError
from werkzeug.security import check_password_hash, generate_password_hash

from helpers import apology, login_required, lookup, usd

# Configure application
app = Flask(__name__)

# Ensure templates are auto-reloaded
app.config["TEMPLATES_AUTO_RELOAD"] = True

# Ensure responses aren't cached
@app.after_request
def after_request(response):
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Expires"] = 0
    response.headers["Pragma"] = "no-cache"
    return response

# Custom filter
app.jinja_env.filters["usd"] = usd

# Configure session to use filesystem (instead of signed cookies)
app.config["SESSION_FILE_DIR"] = mkdtemp()
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# Configure CS50 Library to use SQLite database
db = SQL("sqlite:///finance.db")

# Make sure API key is set
if not os.environ.get("API_KEY"):
    raise RuntimeError("API_KEY not set")


@app.route("/")
@login_required
def index():
    """Show portfolio of stocks"""
    #find current users
    users = db.execute("SELECT cash FROM users WHERE id =:user_id", user_id = session["user_id"])
    stocks = db.execute("SELECT symbol, SUM(shares) as sum_shares FROM transactions WHERE user_id =:user_id GROUP BY symbol HAVING sum_shares > 0", user_id=session["user_id"])
    quotes = {}

    for stock in stocks:
        quotes[stock["symbol"]] = lookup(stock["symbol"])

    balance = users[0]["cash"]
    total = balance


    return render_template("index.html", quotes=quotes, stocks=stocks, total=total, balance=balance)

@app.route("/buy", methods=["GET", "POST"])
@login_required
def buy():
    """Buy shares of stock"""
    if request.method == "POST":
        quote = lookup(request.form.get("symbol"))

        # Check if the symbol exists
        if quote == None:
            return apology("invalid symbol")


        shares = int(request.form.get("shares"))

        # Query database for username
        rows = db.execute("SELECT cash FROM users WHERE id = :user_id", user_id=session["user_id"])

        # How much money the user still has in her account
        balance = rows[0]["cash"]
        price_per_share = quote["price"]

        # Calculate the price of requested shares
        total_price = price_per_share * shares

        if total_price > balance:
            return apology("not enough cash")

        # Book keeping (TODO: should be wrapped with a transaction)
        db.execute("UPDATE users SET cash = cash - :price WHERE id = :user_id", price=total_price, user_id=session["user_id"])
        db.execute("INSERT INTO transactions (user_id, symbol, shares, price_per_share) VALUES(:user_id, :symbol, :shares, :price)",
                   user_id=session["user_id"],
                   symbol=request.form.get("symbol"),
                   shares=shares,
                   price=price_per_share)

        flash("Bought!")

        return redirect("/")

    else:
        return render_template("buy.html")



@app.route("/history")
@login_required
def history():
    """Show history of transactions"""
    transactions = db.execute("SELECT symbol, shares, price_per_share, date FROM transactions WHERE user_id= :user_id ORDER BY date ASC", user_id=session["user_id"])

    return render_template("history.html", transactions=transactions)

@app.route("/login", methods=["GET", "POST"])
def login():
    """Log user in"""

    # Forget any user_id
    session.clear()

    # User reached route via POST (as by submitting a form via POST)
    if request.method == "POST":

        # Ensure username was submitted
        if not request.form.get("username"):
            return apology("must provide username", 403)

        # Ensure password was submitted
        elif not request.form.get("password"):
            return apology("must provide password", 403)

        # Query database for username
        rows = db.execute("SELECT * FROM users WHERE username = :username",
                          username=request.form.get("username"))

        # Ensure username exists and password is correct
        if len(rows) != 1 or not check_password_hash(rows[0]["hash"], request.form.get("password")):
            return apology("invalid username and/or password", 403)

        # Remember which user has logged in
        session["user_id"] = rows[0]["id"]

        # Redirect user to home page
        return redirect("/")

    # User reached route via GET (as by clicking a link or via redirect)
    else:
        return render_template("login.html")


@app.route("/logout")
def logout():
    """Log user out"""

    # Forget any user_id
    session.clear()

    # Redirect user to login form
    return redirect("/")


@app.route("/quote", methods=["GET", "POST"])
@login_required
def quote():
    """Get stock quote."""

    if request.method == "POST":
        quote = lookup(request.form.get("symbol"))

        if quote == None:
            return apology("invalid symbol")

        return render_template("quoted.html", quote=quote)

    else:
        return render_template("quote.html")


@app.route("/register", methods=["GET", "POST"])
def register():
    """Register user"""

    # Forget user_id
    session.clear()

    if request.method == "POST":

        #check if username was submitted
        if not request.form.get("username"):
            return apology("must provide username")

        #check if password was submitted
        elif not request.form.get("password"):
            return apology("must provide password")

        #check if password_confirmation was submmitted
        elif not request.form.get("password_confirmation"):
            return apology("confirm your password")
        #check if password match with password_confirmation
        if request.form.get("password") != request.form.get("password_confirmation"):
            return apology("password must be same")
        #insert new user into database
        hash = generate_password_hash(request.form.get("password"))
        new_user = db.execute("INSERT INTO users (username, hash) VALUES (:username, :hash)",
                            username=request.form.get("username"),
                            hash=hash)

        if not new_user:
            return apology ("username taken")

        session["user_id"] = new_user

        flash("registered!")

        return redirect("/")

    else:
        return render_template("register.html")



@app.route("/add_cash", methods=["GET", "POST"])
@login_required
def add_cash():
    """allow users to add cash"""
    if request.method == "POST":
        add = float(request.form.get("add"))

        db.execute("UPDATE users SET cash = cash + :add WHERE id= :user_id", user_id=session["user_id"], add=add)

        flash("ADDED!")

        return redirect("/")
    else:
        return render_template("add_cash.html")


@app.route("/change_password", methods=["GET", "POST"])
@login_required
def change_password():
    """Allow user to change password"""

    if request.method =="POST":

        if not request.form.get("old_password"):
            return apology("provide your old password")

        rows = db.execute("SELECT hash FROM users WHERE id = :user_id", user_id = session["user_id"])

        if len(rows) != check_password_hash(rows[0]["hash"], request.form.get("old_password")):
            return apology("type your old pasword")

        elif not request.form.get("new_password"):
            return apology("please type your new password")

        elif not request.form.get("password_again"):
            return apology("please confirm your new password")

        elif request.form.get("new_password") != request.form.get("password_again"):
            return apology("new password must match")

        elif request.form.get("old_password") == request.form.get("new_password"):
            return apology("cannot use your old password")

        hash = generate_password_hash(request.form.get("new_password"))
        rows = db.execute("UPDATE users SET hash = :hash WHERE id = :user_id", user_id=session["user_id"], hash=hash)

        flash("CHANGED!")

    return render_template("change_password.html")

@app.route("/sell", methods=["GET", "POST"])
@login_required
def sell():
    """Sell shares of stock"""
    if request.method == "POST":
        quote = lookup(request.form.get("symbol"))

        # Check if the symbol exists
        if quote == None:
            return apology("invalid symbol")


        shares = int(request.form.get("shares"))

        # check if the user have enough shares
        stock = db.execute("SELECT SUM(shares) as sum_shares FROM transactions Where user_id = :user_id AND symbol = :symbol GROUP BY symbol",user_id = session["user_id"], symbol = request.form.get("symbol"))

        if len(stock) <= 0 or stock[0]["sum_shares"] < shares:
            return apology ("you cannot sell shares more than owned")

        rows = db.execute("SELECT cash FROM users WHERE id = :user_id", user_id=session["user_id"])


        # How much money the user still has in her account
        balance = rows[0]["cash"]
        price_per_share = quote["price"]

        # Calculate the price of requested shares
        total_price = price_per_share * shares


        db.execute("UPDATE users SET cash = cash + :price WHERE id = :user_id", price=total_price, user_id=session["user_id"])
        db.execute("INSERT INTO transactions (user_id, symbol, shares, price_per_share) VALUES(:user_id, :symbol, :shares, :price)",
                   user_id=session["user_id"],
                   symbol=request.form.get("symbol"),
                   shares=-shares,
                   price=price_per_share)

        flash("Sold!!")

        return redirect("/")

    else:
        stocks = db.execute("SELECT symbol, SUM(shares) as sum_shares FROM transactions WHERE user_id = :user_id GROUP BY symbol HAVING sum_shares >0", user_id=session["user_id"])
        return render_template("sell.html", stocks=stocks)


def errorhandler(e):
    """Handle error"""
    if not isinstance(e, HTTPException):
        e = InternalServerError()
    return apology(e.name, e.code)


# Listen for errors
for code in default_exceptions:
    app.errorhandler(code)(errorhandler)
